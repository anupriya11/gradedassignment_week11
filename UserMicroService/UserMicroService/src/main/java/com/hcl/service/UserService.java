package com.hcl.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bean.User;
import com.hcl.dao.UserDao;
@Service
public class UserService {
	@Autowired
	UserDao userDao;
    
	public String signUpUser(User user) {
		if(userDao.existsById(user.getUserid())) {
			return "User already exists, Try different Id";
		}
		else {
			userDao.save(user);
			return "User Registered Successfully";
		}
	}
	
	public String signInUser(User user) {
		if(userDao.existsById(user.getUserid())) {
			return "Welcome User, "+user.getUsername()+" you have logged In successfully";
		}
		else {
			return "Incorrect credentials";
		}
	}


}
