package com.hcl.bean;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {
	@Id
	public int userid;
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String username;
	public String usermail;
	public String userpassword;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsermail() {
		return usermail;
	}
	public void setUsermail(String usermail) {
		this.usermail = usermail;
	}
	public String getUserpassword() {
		return userpassword;
	}
	public void setUserpassword(String userpassword) {
		this.userpassword = userpassword;
	}
	@Override
	public String toString() {
		return "User [userid=" + userid + ", username=" + username + ", usermail=" + usermail + ", userpassword="
				+ userpassword + "]";
	}
	
	
}
