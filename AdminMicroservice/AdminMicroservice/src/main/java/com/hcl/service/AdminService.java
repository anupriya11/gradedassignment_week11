package com.hcl.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bean.Admin;
import com.hcl.dao.AdminDao;

@Service
public class AdminService {
	@Autowired
	   AdminDao adminDao;
		
		public String signUp(Admin admin) {
		    Admin ad = adminDao.findByEmailAndPassword(admin.getEmail(), admin.getPassword());
			if(Objects.nonNull(ad)) {
			   return "mail Already Exists, Try with different Mail";
			  
		   }
		   else {
			   adminDao.save(admin);
			   return " Registered Successfully";
		   }
		}
		
		public String signIn(Admin admin) {
			
			Admin adm = adminDao.findByEmailAndPassword(admin.getEmail(), admin.getPassword());
			if(Objects.nonNull(adm)) {
				int len = admin.getEmail().length()-10;
				return "Welcome admin "+admin.getEmail().substring(0,len)+"You are Logged IN Successfully ";
			}
			else {
				return "Incorrect Password or Username";
			}
		}


}
