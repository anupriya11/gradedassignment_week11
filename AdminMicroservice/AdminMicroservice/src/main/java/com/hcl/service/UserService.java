package com.hcl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bean.User;
import com.hcl.dao.UserDao;

@Service
public class UserService {
	
	
	@Autowired
	UserDao userDao;
	
	public String createUser(User user) {
		
		if(userDao.existsById(user.getUserid())) {
			return "This is ID already Exists, Please Give different ID";
		}
		else {
			userDao.save(user);
			return "User Created successfully";
		}
	}
	
	public List<User> retrieveUser(){
		return userDao.findAll();
	}
	
	public String updateUser(User user) {
		
		if(!userDao.existsById(user.getUserid())) {
			return "This ID not Exist, Please give correct ID";
		}
		else {
			User us = userDao.getById(user.getUserid());
			us.setUsermail(user.getUsermail());
			us.setUsername(user.getUsername());
			us.setUserpassword(user.getUserpassword());
			
			userDao.save(us);
			return "User Details updated Successfully";
		}
	}
	
	public String deleteUser(int userid) {
		if(!userDao.existsById(userid)) {
			return "This ID not exists, Please give Valid ID";
		}
		else {
			userDao.deleteById(userid);
			return "User in the Id number "+userid+" has Deleted successfully";
		}
	}
	


}

