package com.hcl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bean.Book;
import com.hcl.dao.BookDao;
@Service
public class BookService {
	@Autowired
	BookDao bookDao;
	
	
     public String createBook(Book book) {
		
		if(bookDao.existsById(book.getBookid())) {
			return "This ID already Exists, Please Give different ID";
		}
		else {
			bookDao.save(book);
			return "Book Created successfully";
		}
	}
	
	public List<Book> retrieveBooks(){
		return bookDao.findAll();
	}
	
	public String updateBook(Book book) {
		
		if(!bookDao.existsById(book.getBookid())) {
			return "This ID not Exist, Please give correct ID";
		}
		else {
			Book bk = bookDao.getById(book.getBookid());
			bk.setBookname(book.getBookname());
			bk.setBookauthor(book.getBookauthor());
			bk.setBookprice(book.getBookprice());
			bookDao.save(bk);
			return "Book Details updated Successfully";
		}
	}
	
	public String deleteBook(int id) {
		if(!bookDao.existsById(id)) {
			return "This ID not exists, Please give Valid ID";
		}
		else {
			bookDao.deleteById(id);
			return "Book in the Id number "+id+" has Deleted successfully";
		}
	}


}
