package com.hcl.dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.bean.User;
@Repository
public interface UserDao extends JpaRepository<User, Integer> {

}
