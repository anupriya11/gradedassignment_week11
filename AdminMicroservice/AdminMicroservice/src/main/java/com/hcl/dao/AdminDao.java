package com.hcl.dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.bean.Admin;
@Repository
public interface AdminDao extends JpaRepository<Admin, String>{
	public Admin findByEmailAndPassword(@Param("email") String user,@Param("password") String pass);

}
