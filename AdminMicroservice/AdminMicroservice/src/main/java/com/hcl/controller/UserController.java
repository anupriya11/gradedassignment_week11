package com.hcl.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.bean.User;
import com.hcl.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	UserService userService;
	
	@PostMapping(value = "createUser",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String CreateUserInfo(@RequestBody User user) {
		return userService.createUser(user);
	}
	
	@GetMapping(value="retrieveUsers",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<User> retrieveUserDetails(){
		return userService.retrieveUser();
	}
	
	@PatchMapping(value="updateUser")
	public String updateUserdetails(@RequestBody User user) {
		return userService.updateUser(user);
		
	}
	
	@DeleteMapping(value="deleteUser/{userid}")
	public String deleteUserDetails(@PathVariable("userid") int userid) {
		return userService.deleteUser(userid);
	}


}
