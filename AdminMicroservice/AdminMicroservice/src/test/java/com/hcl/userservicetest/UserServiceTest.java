package com.hcl.userservicetest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.hcl.bean.User;
import com.hcl.dao.UserDao;
import com.hcl.service.UserService;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

	@Mock
	UserDao userDao;
	
	@InjectMocks 
	UserService userService;
	
	
	@Test
	void testCreateUser() {
         Mockito.when(userDao.existsById(0)).thenReturn(false);
		
		String result=userService.createUser(new User());
		Assertions.assertEquals("User Created successfully",result);

	}

	@Test
	@Disabled
	void testRetrieveUser() {
		
	}

	@Test
	void testUpdateUser() {
		 Mockito.when(userDao.existsById(0)).thenReturn(false);
			String result=userService.updateUser(new User());
			Assertions.assertEquals("This ID not Exist, Please give correct ID",result);
	}

	@Test
	void testDeleteUser() {
		 Mockito.when(userDao.existsById(0)).thenReturn(true);
			
			String result=userService.deleteUser(0);
			Assertions.assertEquals("User in the Id number 0 has Deleted successfully",result);
	}



}
