package com.hcl.bookservicetest;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.hcl.bean.Book;
import com.hcl.dao.BookDao;
import com.hcl.service.BookService;

@ExtendWith(MockitoExtension.class)
class BookServiceTest {

	
	@Mock
	BookDao bookDao;
	
	@InjectMocks
	BookService bookService;
	
	
	@Test
	void testCreateBook() {
	//	fail("Not yet implemented");
		Mockito.when(bookDao.existsById(0)).thenReturn(false);
		
		String result=bookService.createBook(new Book());
		Assertions.assertEquals("Book Created successfully",result);

	}
	@Test
	void testRetrieveBooks() {
		//fail("Not yet implemented");
		List<Book> listOfBooks = new ArrayList<>();
		
		listOfBooks.add(new Book(103,"Lord Of The Rings","Kelvin Jhonse","1600"));
		listOfBooks.add(new Book(104,"Hitler Story","Greigur wince","1280"));
		Mockito.when(bookDao.findAll()).thenReturn(listOfBooks);
		Assertions.assertTrue(bookService.retrieveBooks().get(0).getBookid()==103);
		Assertions.assertTrue(bookService.retrieveBooks().get(0).getBookname().equals("Lord Of The Rings"));
		Assertions.assertTrue(bookService.retrieveBooks().get(0).getBookauthor().equals("Kelvin Jhonse"));
		Assertions.assertTrue(bookService.retrieveBooks().get(0).getBookprice()=="1600");
	}
	@Test
	void testUpdateBook() {
		//fail("Not yet implemented");
        Mockito.when(bookDao.existsById(0)).thenReturn(false);
		
		String result=bookService.updateBook(new Book());
		Assertions.assertEquals("This ID not Exist, Please give correct ID",result);

	}

	@Test
	void testDeleteBook() {
		//fail("Not yet implemented");
        Mockito.when(bookDao.existsById(0)).thenReturn(true);
		
		String result=bookService.deleteBook(0);
		Assertions.assertEquals("Book in the Id number 0 has Deleted successfully",result);		
	}



}
